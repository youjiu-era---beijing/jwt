<?php
namespace YouJiu\Jwt;


use RuntimeException;

class Jwt
{
    protected $publicKey;

    protected $head;
    protected $payload;
    protected $sign;
    protected $jwt;
    protected $data = [];

    /**
     * Jwt constructor..
     * @param $publicKeyPath
     */
    public function __construct($jwt, $publicKeyPath = __DIR__.'/rsa_public_key.pem')
    {
        $this->publicKey = file_get_contents($publicKeyPath);
        $jwt = str_replace('Bearer ', '', $jwt);
        $this->setToken($jwt);
    }

    public function __destruct()
    {
        header('Authorization:Bearer '.$this->jwt);
    }

    public function getToken()
    {
        return $this->jwt;
    }

    /**
     * @param $jwt
     * @return $this
     */
    protected function setToken($jwt){
        $this->jwt = $jwt;
        $jwt = explode('.', $jwt);
        if (count($jwt) != 3) {
            throw new RuntimeException('Error Token!');
        }
        list($head, $payload, $sign) = $jwt;
        $this->head = $head;
        $this->payload = $payload;
        $this->sign = $sign;
        $this->data = $this->getPayload();

    }


    /**
     * 获取payload信息
     * @return mixed
     */
    protected function getPayload()
    {
        return $this->jsonDecode($this->base64UrlDecode($this->payload));
    }


    /**
     * 验证签名
     */
    public function verify(){
        if ($this->data['exp'] < time()){
            $this->refresh();
        }

        $res = openssl_get_publickey($this->publicKey);
        if ($res) {
            $result = (bool)openssl_verify($this->head.'.'.$this->payload, $this->base64UrlDecode($this->sign), $res,OPENSSL_ALGO_SHA256);
            openssl_free_key($res);
            if (! $result){
                throw new RuntimeException('Unauthorized!');
            }
        } else {
            throw new RuntimeException('The public key format is wrong!');
        }
        header('Authorization:Bearer '.$this->jwt);
        return $this;
    }

    /**
     * 获取Payload数据
     */
    public function getPayloadData()
    {
        return $this->data;
    }

    protected function refresh()
    {
        $url = 'https://ucenter.new150.com/api/auth/refresh';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Authorization: Bearer " . $this->jwt
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response, true);
        if ($data['code'] != 200) {
            throw new RuntimeException($data['message']);
        }
        $jwt = $data['data']['access_token'];
        $this->setToken($jwt);
    }

    /**
     * Decodes from JSON, validating the errors (will return an associative array
     * instead of objects)
     *
     * @param string $json
     * @return mixed
     *
     * @throws RuntimeException When something goes wrong while decoding
     */
    protected function jsonDecode($json)
    {
        $data = json_decode($json, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            throw new RuntimeException('Error while decoding to JSON: ' . json_last_error_msg());
        }

        return $data;
    }

    /**
     * Decodes from base64url
     *
     * @param string $data
     * @return string
     */
    protected function base64UrlDecode($data)
    {
        if ($remainder = strlen($data) % 4) {
            $data .= str_repeat('=', 4 - $remainder);
        }

        return base64_decode(strtr($data, '-_', '+/'));
    }

}
